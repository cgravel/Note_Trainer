#Region Initial optionset
#include <GUIConstants.au3>
#include <WindowsConstants.au3>
Opt("pixelcoordmode", 2)
Opt("mousecoordmode", 2)
Opt("WinTitleMatchMode", 4)
$title_c = "Note Trainer"
#EndRegion Initial optionset

#Region Global Variables
Dim $button[12]
Dim $CountBy[4]
Dim $DoOver[7][2]
Global $Answer = 13
Global $randomSemitoneMin = 0
Global $randomSemitoneMax = 12
Global $randomNote = 0
Global $randomSemitone = Random($randomSemitoneMin, $randomSemitoneMax, 1)
Global $guimaxx = 240
Global $guimaxy = 320
Global $counterTotal_Var = 0
Global $counterWin_Var = 0
Global $counterLoss_Var = 0
Global $msg
Global $FretboardString
Global $semitoneCountingFactor
Global $timerInit
Global $timerDiff
Global $timerVar = 3000

#EndRegion Global Variables
;HotKeySet("r", "EnableHotkeys")
;HotKeySet("t", "DisableHotkeys")

#Region GUI
#Region Test Tab

GUICreate("Note Trainer GUI", $guimaxx, $guimaxy)
GUICtrlCreateTab(0, 0, 240, 30)
Global $test = GUICtrlCreateTabItem("Test")

$mainmenu = GUICtrlCreateMenu("Start")
GUICtrlCreateMenuItem("Exit", -1)
GUISetOnEvent(-1, "Quit")
GUICtrlSetOnEvent($GUI_EVENT_CLOSE, "Quit")
GUISetOnEvent($GUI_EVENT_CLOSE, "Quit")

$button[0] = GUICtrlCreateButton("A", 0, 220, 40, 40)
$button[1] = GUICtrlCreateButton("A#/Bb", 40, 220, 40, 40)
$button[2] = GUICtrlCreateButton("B", 80, 220, 40, 40)
$button[3] = GUICtrlCreateButton("C", 120, 220, 40, 40)
$button[4] = GUICtrlCreateButton("C#/Db", 160, 220, 40, 40)
$button[5] = GUICtrlCreateButton("D", 200, 220, 40, 40)
$button[6] = GUICtrlCreateButton("D#/Eb", 0, 260, 40, 40)
$button[7] = GUICtrlCreateButton("E/Fb", 40, 260, 40, 40)
$button[8] = GUICtrlCreateButton("E#/F", 80, 260, 40, 40)
$button[9] = GUICtrlCreateButton("F#/Gb", 120, 260, 40, 40)
$button[10] = GUICtrlCreateButton("G", 160, 260, 40, 40)
$button[11] = GUICtrlCreateButton("G#/Ab", 200, 260, 40, 40)

$revealAnswerCheckBox = GUICtrlCreateCheckbox("Reveal Answer", 10, 130, 100, 20)
$revealAnswer = GUICtrlCreateLabel("*****", 110, 130, 100, 20)
$equationInWiting = GUICtrlCreateLabel("", 10, 150, 200, 20)

GUICtrlCreateLabel("Hotkeys are", 10, 90, 60, 20)
Global $hotkeyStatus = "Disabled"
$hotkeyButton = GUICtrlCreateButton($hotkeyStatus,70,85,50,20)


$refresh = GUICtrlCreateButton("Refresh", 0, 30, 50, 40)

$reply = GUICtrlCreateLabel("Null", 60, 70, 200, 20)
GUICtrlCreateLabel("Note", 60, 30, 30, 20)
$note = GUICtrlCreateLabel("", 60, 50, 40, 20)
GUICtrlCreateLabel("Semitones +/-", 100, 30, 80, 20)
$semitones = GUICtrlCreateLabel("", 100, 50, 20, 20)

$upSemitones = GUICtrlCreateButton("Up", 180, 30, 40, 20)
GUICtrlSetOnEvent(-1, "upSemitones")
$downSemitones = GUICtrlCreateButton("Down", 180, 50, 40, 20)
GUICtrlSetOnEvent(-1, "downSemitones")

GUICtrlCreateLabel("Correct", 10, 180, 50, 20)
GUICtrlCreateLabel("Incorrect", 70, 180, 50, 20)
GUICtrlCreateLabel("Total", 140, 180, 50, 20)
$counterWin = GUICtrlCreateLabel("0", 10, 200, 50, 20)
$counterLoss = GUICtrlCreateLabel("0", 70, 200, 50, 20)
$counterTotal = GUICtrlCreateLabel("0", 140, 200, 40, 20)

$clearScores = GUICtrlCreateButton("Clear", 180, 200, 60, 20)

#EndRegion Test Tab
#Region Note Sum Table Tab
GUICtrlCreateTabItem("Note Sum Tables")

$dispFretboard = GUICtrlCreateButton("Show Fretboard", 160, 160, 80, 30)
$closeFretboard = GUICtrlCreateButton("Close Fretboard", 160, 190, 80, 30)

Dim $NoteSumTable[12]
$NoteSumTable[0] = GUICtrlCreateButton("A", 0, 220, 40, 40)
$NoteSumTable[1] = GUICtrlCreateButton("A#/Bb", 40, 220, 40, 40)
$NoteSumTable[2] = GUICtrlCreateButton("B", 80, 220, 40, 40)
$NoteSumTable[3] = GUICtrlCreateButton("C", 120, 220, 40, 40)
$NoteSumTable[4] = GUICtrlCreateButton("C#/Db", 160, 220, 40, 40)
$NoteSumTable[5] = GUICtrlCreateButton("D", 200, 220, 40, 40)
$NoteSumTable[6] = GUICtrlCreateButton("D#/Eb", 0, 260, 40, 40)
$NoteSumTable[7] = GUICtrlCreateButton("E", 40, 260, 40, 40)
$NoteSumTable[8] = GUICtrlCreateButton("F", 80, 260, 40, 40)
$NoteSumTable[9] = GUICtrlCreateButton("F#/Gb", 120, 260, 40, 40)
$NoteSumTable[10] = GUICtrlCreateButton("G", 160, 260, 40, 40)
$NoteSumTable[11] = GUICtrlCreateButton("G#/Ab", 200, 260, 40, 40)

$NoteSumTableTop = GUICtrlCreateGroup("Note Sum Table for ",5,30,140,185);, $WS_THICKFRAME)
$NoteSumTableDisplayLabelBox = GUICtrlCreateLabel("",10,49,180,200) ;#hererer

GUICtrlCreateTabItem("Options")
$staticSemitones = GUICtrlCreateCheckbox("Static Semitones", 10, 30, 100, 20)
$SubjectWholeNotes = GUICtrlCreateCheckbox("Subject Whole Notes", 10, 50, 130, 20)
GUICtrlSetTip(-1, "This disallows sharps and flats for the subject notes. NOTE: This only takes affect after the current Selection.", "Subject Whole Notes")
$AnswerWholeNotes = GUICtrlCreateCheckbox("Answer Whole Notes", 10, 70, 130, 20)
GUICtrlSetTip(-1, "This disallows sharps and flats for answers. NOTE: This only takes affect after the current Selection.", "Answer Whole Notes")
$TargetedLearning = GUICtrlCreateCheckbox("Targeted Learning", 10, 90, 105, 20)
$TargetedLearningTimerVar = GUICtrlCreateInput("3000", 150, 90, 60, 20)
$TargetedLearningLoader = GUICtrlCreateButton("<- m/s", 115, 90, 35, 20)


$semitoneCounting = GUICtrlCreateCheckbox("Semitone Counting", 10, 215, 110, 20)

GUICtrlCreateGroup("# of Semitones", 10, 240, 140, 50)
$CountBy[0] = GUICtrlCreateRadio("3's", 20, 255, 30); A3C B3D C4E D3F E3G F4A G4B
GUICtrlSetState(-1, $GUI_CHECKED)
$CountBy[1] = GUICtrlCreateRadio("5's", 50, 255, 30) ; A5D B5E C5F D5G E5A F6B G5C  (B6F is missed)
$CountBy[2] = GUICtrlCreateRadio("7's", 80, 255, 30) ; A7E B8G C7G D7A E7B F7C G7D
$CountBy[3] = GUICtrlCreateRadio("9's", 110, 255, 30) ; A10G B10A C9A D9B E10D F9D G9E (A8F and E8C are missed)
GUICtrlCreateGroup("", -99, -99, 1, 1) ;close group

For $i = 0 To 3
	GUICtrlSetState($CountBy[$i], $GUI_DISABLE)
Next

GUISetState()
;HotKeySet("p", "dispshit")
Func dispshit()
	Local $shit
	For $i = 0 To 6
		For $o = 0 To 1
			$shit = $shit & @CR & $i & " , " & $o & " - " & $DoOver[$i][$o]
		Next
	Next
	MsgBox(0, "", $shit)
EndFunc   ;==>dispshit
#EndRegion Note Sum Table Tab
#EndRegion GUI
#Region GUI-functions
Func EnableHotkeys()
	HotKeySet("a", "a")
	HotKeySet("b", "b")
	HotKeySet("c", "c")
	HotKeySet("d", "d")
	HotKeySet("e", "e")
	HotKeySet("f", "f")
	HotKeySet("g", "g")
	$hotkeyStatus = "Enabled"
EndFunc   ;==>EnableHotkeys


Func DisableHotkeys()
	HotKeySet("a")
	HotKeySet("b")
	HotKeySet("c")
	HotKeySet("d")
	HotKeySet("e")
	HotKeySet("f")
	HotKeySet("g")
	$hotkeyStatus = "Disabled"
EndFunc   ;==>DisableHotkeys

Func a()
	$msg = $button[0]
EndFunc   ;==>a

Func b()
	$msg = $button[2]
EndFunc   ;==>b

Func c()
	$msg = $button[3]
EndFunc   ;==>c

Func d()
	$msg = $button[5]
EndFunc   ;==>d

Func e()
	$msg = $button[7]
EndFunc   ;==>e

Func f()
	$msg = $button[8]
EndFunc   ;==>f

Func g()
	$msg = $button[10]
EndFunc   ;==>g

Func upSemitones()
	If $randomSemitone < 12 Then
		$randomSemitone = $randomSemitone + 1
		GUICtrlSetData($semitones, $randomSemitone)
	EndIf
EndFunc   ;==>upSemitones

Func downSemitones()

	If $randomSemitone > -12 Then
		$randomSemitone = $randomSemitone - 1
		GUICtrlSetData($semitones, $randomSemitone)
	EndIf
EndFunc   ;==>downSemitones

Func Quit()
	Exit
EndFunc   ;==>Quit

#EndRegion GUI-functions


#Region Functions
Func PlusOrMinus()
	If $randomSemitone < 0 Then
		Return " minus "
	Else
		Return " plus "
	EndIf
EndFunc   ;==>PlusOrMinus

Func transcribenumbertonote($num)
	Select
		Case $num > 11
			$num = $num - 12
		Case $num < 0
			$num = $num + 12
	EndSelect
	Select
		Case $num = 0
			Return "A"
		Case $num = 1
			Return "A#/Bb"
		Case $num = 2
			Return "B"
		Case $num = 3
			Return "C"
		Case $num = 4
			Return "C#/Db"
		Case $num = 5
			Return "D"
		Case $num = 6
			Return "D#/Eb"
		Case $num = 7
			Return "E"
		Case $num = 8
			Return "F"
		Case $num = 9
			Return "F#/Gb"
		Case $num = 10
			Return "G"
		Case $num = 11
			Return "G#/Ab"
	EndSelect
EndFunc   ;==>transcribenumbertonote

Func CheckAnswer($Answer)
	Local $rightanswer = $randomNote + $randomSemitone
	Select
		Case $rightanswer > 11
			$rightanswer = $rightanswer - 12
		Case $rightanswer < 0
			$rightanswer = $rightanswer + 12
	EndSelect
	If $rightanswer = $Answer Then
		GUICtrlSetData($reply, "You have choosen wisely!")
		$counterWin_Var = $counterWin_Var + 1
		$DoOver[6][0] = 0
		$DoOver[6][1] = 0
	Else
		$DoOver[6][0] = $randomNote
		$DoOver[6][1] = $randomSemitone
		GUICtrlSetData($reply, "The correct answer WAS " & transcribenumbertonote($rightanswer))
		$counterLoss_Var = $counterLoss_Var + 1
	EndIf
	If (TimerDiff($timerInit) > $timerVar) Then
		$DoOver[6][0] = $randomNote
		$DoOver[6][1] = $randomSemitone
	EndIf
	$counterTotal_Var = $counterTotal_Var + 1
	GUICtrlSetData($counterTotal, $counterTotal_Var)
	GUICtrlSetData($counterLoss, $counterLoss_Var)
	GUICtrlSetData($counterWin, $counterWin_Var)
	If ($DoOver[0][1] <> 0) And ($timerVar <> 1234) Then ; 1234 is code for disabling
		$randomNote = $DoOver[0][0]
		$randomSemitone = $DoOver[0][1]
	Else
		SelectNewNote()
		SelectNewSemitone()
	EndIf
	PushDoOver()
	If (GUICtrlRead($revealAnswerCheckBox) = $GUI_CHECKED) Then
		GUICtrlSetData($revealAnswer, transcribenumbertonote($randomNote + $randomSemitone))
		GUICtrlSetData($equationInWiting, transcribenumbertonote($randomNote) & PlusOrMinus() & Abs($randomSemitone) & " equals " & transcribenumbertonote($randomNote + $randomSemitone))
	EndIf
	GUICtrlSetData($note, transcribenumbertonote($randomNote))
	GUICtrlSetData($semitones, $randomSemitone)
	$timerInit = TimerInit()
EndFunc   ;==>CheckAnswer

; 0 - A... 1,4,6,9,11 are the flats/sharps.
; Inversly 0,2,3,5,7,8,10 are the Whole notes.

Func SelectNewNote() ; options to consider for this are : semitone counting, subjectwholenotes, answerwholenotes.
	$AcceptableNoteChoosen = 0
	While $AcceptableNoteChoosen = 0 ; this repeats if an acceptable note is not choosen, by default an acceptable note is choosen.
		$AcceptableNoteChoosen = 1
		If GUICtrlRead($semitoneCounting) = $GUI_CHECKED Then
			$rightanswer = $randomNote + $randomSemitone
			Select
				Case $rightanswer > 11
					$rightanswer = $rightanswer - 12
				Case $rightanswer < 0
					$rightanswer = $rightanswer + 12
			EndSelect
			If IsFlat($rightanswer) Then
				$randomNote = 3
			Else
				$randomNote = $rightanswer
			EndIf
		Else
			$randomNote = Random(0, 11, 1)
			If GUICtrlRead($SubjectWholeNotes) = $GUI_CHECKED And IsFlat($randomNote) Then
				$AcceptableNoteChoosen = 0
			EndIf
		EndIf
	WEnd
EndFunc   ;==>SelectNewNote

Func SelectNewSemitone()
	$AcceptableNoteChoosen = 0
	While $AcceptableNoteChoosen = 0
		$AcceptableNoteChoosen = 1
		If (GUICtrlRead($semitoneCounting) = $GUI_CHECKED) Then
			$randomSemitone = 3 + (2 * $semitoneCountingFactor)
			$rightanswer = $randomNote + $randomSemitone
			If IsFlat($rightanswer) Then
				$randomSemitone += 1
			EndIf
		Else
			If (GUICtrlRead($staticSemitones) = $GUI_UNCHECKED) Then
				$randomSemitone = Random($randomSemitoneMin, $randomSemitoneMax, 1)
				If GUICtrlRead($AnswerWholeNotes) = $GUI_CHECKED Then
					$rightanswer = $randomNote + $randomSemitone ; - declares rightanswer here only because I needed to find if it was a sharp/flat, which is needed to be known for the options I have
					If IsFlat($rightanswer) Then
						;If ($rightanswer <> 0) And ($rightanswer <> 2) And ($rightanswer <> 3) And ($rightanswer <> 5) And ($rightanswer <> 7) And ($rightanswer <> 8) And ($rightanswer <> 10) Then
						$randomSemitone = Random(0, 11, 1)
						$AcceptableNoteChoosen = 0
						;MsgBox(0,"this time","triggered")
					EndIf
				EndIf

			EndIf
		EndIf
	WEnd
EndFunc   ;==>SelectNewSemitone

Func PushDoOver()
	For $i = 0 To 5
		For $o = 0 To 1
			$DoOver[$i][$o] = $DoOver[($i + 1)][$o]
		Next
	Next
	$DoOver[6][0] = 0
	$DoOver[6][1] = 0
EndFunc   ;==>PushDoOver

Func IsFlat($tempnote)
	Select
		Case $tempnote > 11
			$tempnote = $tempnote - 12
		Case $tempnote < 0
			$tempnote = $tempnote + 12
	EndSelect
	If ($tempnote <> 0) And ($tempnote <> 2) And ($tempnote <> 3) And ($tempnote <> 5) And ($tempnote <> 7) And ($tempnote <> 8) And ($tempnote <> 10) Then
		Return 1
	Else
		Return 0
	EndIf
EndFunc   ;==>IsFlat


Func DisplaySumTable($noteTodisplay)
	Local $sMsg
	For $i = 0 To 11
		$sMsg = $sMsg & transcribenumbertonote($noteTodisplay) & " + " & $i & " = " & transcribenumbertonote($noteTodisplay + $i) & @CR
	Next
	;MsgBox(0, "Note Sum Table for " & transcribenumbertonote($noteTodisplay), $sMsg)
	GUICtrlSetData($NoteSumTableDisplayLabelBox,$sMsg)
	GUICtrlSetData($NoteSumTableTop,"Note Sum Table for " & transcribenumbertonote($noteTodisplay))
	;GUISwitch($gui
EndFunc   ;==>DisplaySumTable

Func DisplayFretboard()
	SplashTextOn("Standard Tuning Guitar Fretboard", $FretboardString, 1200, 130, ((@DesktopWidth - 1200) / 2), (@DesktopHeight - 55 - 130), -1, "Courier")
EndFunc   ;==>DisplayFretboard

Func CloseFretboard()
	SplashOff()
EndFunc   ;==>CloseFretboard

Func MakeFretBoardString(ByRef $sMsg)
	Local $t
	For $o = 1 To 6
		Select
			Case $o = 1 ; $o represents the string from bottom to top and $t represents the note in distance of semitones from A
				$t = 7 ; bottom string E
			Case $o = 2
				$t = 2
			Case $o = 3
				$t = 10
			Case $o = 4
				$t = 5
			Case $o = 5
				$t = 0
			Case $o = 6
				$t = 7
		EndSelect
		; 1, 4, 6, 9, 11  are Sharps
		$sMsg = $sMsg & transcribenumbertonote($t) & "|"
		For $i = 1 To 12
			If ($t + $i) <> 1 And ($t + $i) <> 4 And ($t + $i) <> 6 And ($t + $i) <> 9 And ($t + $i) <> 11 And ($t + $i) <> 13 And ($t + $i) <> 16 And ($t + $i) <> 18 And ($t + $i) <> 21 And ($t + $i) <> 23 Then ; if the current note is not a sharp
				$sMsg = $sMsg & "   " & transcribenumbertonote($t + $i) & "   |"
			Else
				$sMsg = $sMsg & " " & transcribenumbertonote($t + $i) & " |"
			EndIf
			If $i = 12 Then StringTrimRight($sMsg, 3)
		Next
		$sMsg = $sMsg & @CR
	Next
	$sMsg = $sMsg & "  *               *               *               *               *                       **" & @CR
	$sMsg = $sMsg & "  1               3               5               7               9                       12"
EndFunc   ;==>MakeFretBoardString

#EndRegion Functions


#Region MAIN LOOP
For $i = 0 To 6
	For $o = 0 To 1
		$DoOver[$i][$o] = 0
	Next
Next

For $i = 0 To 11
	GUICtrlSetData($button[$i], transcribenumbertonote($i))
Next
GUICtrlSetData($note, transcribenumbertonote($randomNote))
GUICtrlSetData($semitones, $randomSemitone)
MakeFretBoardString($FretboardString)

While 1
	$msg = GUIGetMsg()
	Select
		Case $msg = $GUI_EVENT_CLOSE
			ExitLoop
		Case $msg = $button[0]
			$Answer = 0
		Case $msg = $button[1]
			$Answer = 1
		Case $msg = $button[2]
			$Answer = 2
		Case $msg = $button[3]
			$Answer = 3
		Case $msg = $button[4]
			$Answer = 4
		Case $msg = $button[5]
			$Answer = 5
		Case $msg = $button[6]
			$Answer = 6
		Case $msg = $button[7]
			$Answer = 7
		Case $msg = $button[8]
			$Answer = 8
		Case $msg = $button[9]
			$Answer = 9
		Case $msg = $button[10]
			$Answer = 10
		Case $msg = $button[11]
			$Answer = 11
		Case $msg = $upSemitones
			upSemitones()
			If (GUICtrlRead($revealAnswerCheckBox) = $GUI_CHECKED) Then
				GUICtrlSetData($revealAnswer, transcribenumbertonote($randomNote + $randomSemitone))
				GUICtrlSetData($equationInWiting, transcribenumbertonote($randomNote) & PlusOrMinus() & Abs($randomSemitone) & " equals " & transcribenumbertonote($randomNote + $randomSemitone))
			EndIf
		Case $msg = $downSemitones
			downSemitones()
			If (GUICtrlRead($revealAnswerCheckBox) = $GUI_CHECKED) Then
				GUICtrlSetData($revealAnswer, transcribenumbertonote($randomNote + $randomSemitone))
				GUICtrlSetData($equationInWiting, transcribenumbertonote($randomNote) & PlusOrMinus() & Abs($randomSemitone) & " equals " & transcribenumbertonote($randomNote + $randomSemitone))
			EndIf
		Case $msg = $revealAnswerCheckBox
			If (GUICtrlRead($revealAnswerCheckBox) = $GUI_CHECKED) Then
				GUICtrlSetData($revealAnswer, transcribenumbertonote($randomNote + $randomSemitone))
				GUICtrlSetData($equationInWiting, transcribenumbertonote($randomNote) & PlusOrMinus() & Abs($randomSemitone) & " equals " & transcribenumbertonote($randomNote + $randomSemitone))
			Else
				GUICtrlSetData($revealAnswer, "*****")
				GUICtrlSetData($equationInWiting, "")
			EndIf
		Case $msg = $refresh
			$randomNote = 3
			GUICtrlSetData($note, transcribenumbertonote($randomNote))
			$randomSemitone = 0
			GUICtrlSetData($semitones, $randomSemitone)

		Case $msg = $hotkeyButton
			If $hotkeyStatus = "Disabled" Then
				EnableHotkeys()
			Else
				DisableHotkeys()
			EndIf
				GUICtrlSetData($hotkeyButton,$hotkeyStatus)
		Case $msg = $clearScores
			$counterLoss_Var = 0
			GUICtrlSetData($counterLoss, $counterLoss_Var)
			$counterWin_Var = 0
			GUICtrlSetData($counterWin, $counterWin_Var)
			$counterTotal_Var = 0
			GUICtrlSetData($counterTotal, $counterTotal_Var)

		Case $msg = $staticSemitones
			If (GUICtrlRead($staticSemitones) = $GUI_CHECKED) Then
				GUICtrlSetState($AnswerWholeNotes, $GUI_UNCHECKED)
				GUICtrlSetState($AnswerWholeNotes, $GUI_DISABLE)
			Else
				GUICtrlSetState($AnswerWholeNotes, $GUI_ENABLE)
			EndIf
		Case $msg = $semitoneCounting
			If (GUICtrlRead($semitoneCounting) = $GUI_CHECKED) Then
				GUICtrlSetState($SubjectWholeNotes, $GUI_UNCHECKED)
				GUICtrlSetState($SubjectWholeNotes, $GUI_DISABLE)
				GUICtrlSetState($staticSemitones, $GUI_UNCHECKED)
				GUICtrlSetState($staticSemitones, $GUI_DISABLE)
				GUICtrlSetState($AnswerWholeNotes, $GUI_UNCHECKED)
				GUICtrlSetState($AnswerWholeNotes, $GUI_DISABLE)
				For $i = 0 To 3
					GUICtrlSetState($CountBy[$i], $GUI_ENABLE)
					If GUICtrlRead($CountBy[$i]) = $GUI_CHECKED Then
						$semitoneCountingFactor = $i
					EndIf
				Next
			Else

				For $i = 0 To 3
					GUICtrlSetState($CountBy[$i], $GUI_DISABLE)
				Next
				GUICtrlSetState($SubjectWholeNotes, $GUI_ENABLE)
				GUICtrlSetState($staticSemitones, $GUI_ENABLE)
				GUICtrlSetState($AnswerWholeNotes, $GUI_ENABLE)
			EndIf

		Case $msg = $TargetedLearning
			If GUICtrlRead($TargetedLearning) = $GUI_CHECKED Then
				If (StringIsDigit(GUICtrlRead($TargetedLearningTimerVar)) = 0) Or (GUICtrlRead($TargetedLearningTimerVar) < 0) Then
					MsgBox(0, "Error", "The field must contain only digits and must be a positive integer")
					GUICtrlSetState($TargetedLearning, $GUI_UNCHECKED)
				Else
					$timerVar = GUICtrlRead($TargetedLearningTimerVar)
				EndIf
			EndIf
			If GUICtrlRead($TargetedLearning) = $GUI_UNCHECKED Then
				$timerVar = 1234
			EndIf
		Case $msg = $TargetedLearningLoader
			If (StringIsDigit(GUICtrlRead($TargetedLearningTimerVar)) = 0) Or (GUICtrlRead($TargetedLearningTimerVar) < 0) Then
				MsgBox(0, "Error", "The field must contain only digits and must be a positive integer")
			Else
				$timerVar = GUICtrlRead($TargetedLearningTimerVar)
			EndIf

		Case $msg = $CountBy[0]
			$semitoneCountingFactor = 0

		Case $msg = $CountBy[1]
			$semitoneCountingFactor = 1

		Case $msg = $CountBy[2]
			$semitoneCountingFactor = 2

		Case $msg = $CountBy[3]
			$semitoneCountingFactor = 3

		Case $msg = $NoteSumTable[0]
			DisplaySumTable(0)
		Case $msg = $NoteSumTable[1]
			DisplaySumTable(1)
		Case $msg = $NoteSumTable[2]
			DisplaySumTable(2)
		Case $msg = $NoteSumTable[3]
			DisplaySumTable(3)
		Case $msg = $NoteSumTable[4]
			DisplaySumTable(4)
		Case $msg = $NoteSumTable[5]
			DisplaySumTable(5)
		Case $msg = $NoteSumTable[6]
			DisplaySumTable(6)
		Case $msg = $NoteSumTable[7]
			DisplaySumTable(7)
		Case $msg = $NoteSumTable[8]
			DisplaySumTable(8)
		Case $msg = $NoteSumTable[9]
			DisplaySumTable(9)
		Case $msg = $NoteSumTable[10]
			DisplaySumTable(10)
		Case $msg = $NoteSumTable[11]
			DisplaySumTable(11)

		Case $msg = $dispFretboard
			DisplayFretboard()

		Case $msg = $closeFretboard
			CloseFretboard()

	EndSelect
	If $Answer <> 13 Then
		CheckAnswer($Answer)
		$Answer = 13
	EndIf
WEnd
Exit
;~ While GuiGetMsg() <> $GUI_EVENT_CLOSE
;~ WEnd
#EndRegion MAIN LOOP